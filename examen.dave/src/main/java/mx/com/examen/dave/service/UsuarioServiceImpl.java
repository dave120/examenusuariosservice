package mx.com.examen.dave.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.examen.dave.dao.UsuarioDAO;
import mx.com.examen.dave.entity.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	UsuarioDAO dao;
	
	@Override
	public List<Usuario> listar() {
		return dao.findByEstatus(1);
	}

	@Override
	public Usuario insertar(Usuario u) {
		return dao.save(u);
	}

	@Override
	public Usuario actualizar(Usuario u) {
		System.out.println("Modificar: " + u);
		u.getFechaNacimiento().setDate( u.getFechaNacimiento().getDate() + 1 );
		return dao.save(u);
	}
	
	@Override
	public Usuario baja(Usuario u) {
		u.setEstatus(0);
		return dao.save(u);
	}

	@Override
	public Usuario buscar(Usuario u) {
		return dao.findByNumeroEmpleado(u.getNumeroEmpleado());
	}

}
