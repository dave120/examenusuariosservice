package mx.com.examen.dave.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import mx.com.examen.dave.entity.Usuario;

public interface UsuarioDAO extends JpaRepository<Usuario, Integer> {

	public List<Usuario> findByEstatus(int estatus);
	public Usuario findByNumeroEmpleado(int numeroEmpleado);
	
}
