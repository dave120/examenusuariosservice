package mx.com.examen.dave.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USUARIO")
public class Usuario implements Serializable {

	@Id
	@Column(name="NUMERO_EMPLEADO", length=6)
	int numeroEmpleado;
	
	@Column(name="NOMBRE_COMPLETO_EMPLEADO", length=100)
	String nombreCompletoEmpleado;
	
	@Column(name="FECHA_NACIMIENTO")
	Date fechaNacimiento;
	
	@Column(name="NO_CELULAR" , length=10)
	int noCelular;
	
	@Column(name="ESTATUS" , length=1)
	int estatus;
	
	
	public Usuario() {
		this.estatus = 0;
	}
	
	public Usuario(int numeroEmpleado, String nombreCompletoEmpleado, Date fechaNacimiento, int noCelular, int estatus) {
		this.numeroEmpleado = numeroEmpleado;
		this.nombreCompletoEmpleado = nombreCompletoEmpleado;
		this.fechaNacimiento = fechaNacimiento;
		this.noCelular = noCelular;
		this.estatus = estatus;
	}

	public int getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(int numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public String getNombreCompletoEmpleado() {
		return nombreCompletoEmpleado;
	}

	public void setNombreCompletoEmpleado(String nombreCompletoEmpleado) {
		this.nombreCompletoEmpleado = nombreCompletoEmpleado;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getNoCelular() {
		return noCelular;
	}

	public void setNoCelular(int noCelular) {
		this.noCelular = noCelular;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "Usuario [numeroEmpleado=" + numeroEmpleado + ", nombreCompletoEmpleado=" + nombreCompletoEmpleado
				+ ", fechaNacimiento=" + fechaNacimiento + ", noCelular=" + noCelular + ", estatus=" + estatus + "]";
	}
	
	private static final long serialVersionUID = 1L;
	
	
}
