package mx.com.examen.dave.WS;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.examen.dave.entity.Usuario;
import mx.com.examen.dave.service.IUsuarioService;
import mx.com.examen.dave.service.UsuarioServiceImpl;

@RestController
@CrossOrigin
public class UsuarioWs {

	@Autowired
	IUsuarioService service;
	
//	http://localhost:8000/listar_empleados
	@GetMapping("listar_empleados")
	public List<Usuario> listar() {
		return service.listar();
	}
	
//	http://localhost:8000/insertar_usuario
	@PostMapping("insertar_usuario")
	public Usuario insertar(@RequestBody Usuario user) {
		return service.insertar(user);
	}
	
//	http://localhost:8000/actualiza	
	@PutMapping("actualiza")
	public Usuario actualiza(@RequestBody Usuario user) {
		return service.actualizar(user);
	}
	
//	http://localhost:8000/baja	
	@PutMapping("baja")
	public Usuario baja(@RequestBody Usuario user) {
		return service.baja(user);
	}
	
//	http://localhost:8000/buscar	
	@PostMapping("buscar")
	public Usuario buscar(@RequestBody Usuario user) {
		return service.buscar(user);
	}
	
}
