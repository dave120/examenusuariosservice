package mx.com.examen.dave.service;

import java.util.List;

import mx.com.examen.dave.entity.Usuario;

public interface IUsuarioService {

	public List<Usuario> listar();
	public Usuario buscar(Usuario u);
	public Usuario insertar(Usuario u);
	public Usuario actualizar(Usuario u);
	public Usuario baja(Usuario u);
	
}
